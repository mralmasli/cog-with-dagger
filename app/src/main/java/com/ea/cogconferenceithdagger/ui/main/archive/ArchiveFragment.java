package com.ea.cogconferenceithdagger.ui.main.archive;


import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ea.cogconferenceithdagger.R;
import com.ea.cogconferenceithdagger.databinding.FragmentArchiveBinding;
import com.ea.cogconferenceithdagger.model.archive.ArchiveModel;
import com.ea.cogconferenceithdagger.model.events.RVConferencesModel;
import com.ea.cogconferenceithdagger.ui.main.Resource;
import com.ea.cogconferenceithdagger.ui.main.events.RecyclerViewForEvents;
import com.ea.cogconferenceithdagger.util.PicassoHandler;
import com.ea.cogconferenceithdagger.viewmodel.ViewModelProviderFactory;

import java.util.ArrayList;

import javax.inject.Inject;

import dagger.android.support.DaggerFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class ArchiveFragment extends DaggerFragment {

    private static final String TAG = "ArchiveFragment";

    public ArchiveFragment() {
        // Required empty public constructor
    }

    private FragmentArchiveBinding binding;
    private ArchiveViewModel viewModel;
    private ArrayList<RVConferencesModel> model;

    @Inject
    RecyclerViewForArchive adapterForArchive;

    @Inject
    ViewModelProviderFactory providerFactory;

    @Inject
    PicassoHandler picassoHandler;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater,R.layout.fragment_archive, container, false);
        View view = binding.getRoot();

        viewModel = ViewModelProviders.of(getActivity(),providerFactory).get(ArchiveViewModel.class);

        initRV();

        subscribeArchive();

        return view;
    }

    void subscribeArchive(){
        viewModel.subscribeArchive().removeObservers(getViewLifecycleOwner());
        viewModel.subscribeArchive().observe(getViewLifecycleOwner(), archiveModelResource -> {
            if(archiveModelResource != null){
                switch (archiveModelResource.status){
                    case LOADING:{
                        Log.i(TAG, "subscribeArchive: is loading...");
                        break;
                    }
                    case SUCCESS:{
                        model = new ArrayList<>();
                        model.add(new RVConferencesModel(1,"Conferences",1));
                        for(int i = 0; i < archiveModelResource.data.getUserEventsResult().get(0).getConferences().size();i++){
                            model.add(new RVConferencesModel(2,
                                    archiveModelResource.data.getUserEventsResult().get(0).getConferences().get(i),
                                    1));
                        }
                        model.add(new RVConferencesModel(1,"Events",1));
                        for(int i = 0; i < archiveModelResource.data.getUserEventsResult().get(0).getEvents().size();i++){
                            model.add(new RVConferencesModel(3,
                                    archiveModelResource.data.getUserEventsResult().get(0).getEvents().get(i),
                                    1));
                        }

                        adapterForArchive.setConferencesModels(model);
                        break;
                    }
                    case ERROR:{
                        Log.i(TAG, "subscribeArchive: " + archiveModelResource.message);
                        break;
                    }
                }
            }
        });
    }

    void initRV(){
        adapterForArchive = new RecyclerViewForArchive(picassoHandler);
        binding.rvArchive.setLayoutManager(new LinearLayoutManager(getContext()));
        binding.rvArchive.setAdapter(adapterForArchive);
    }

}
