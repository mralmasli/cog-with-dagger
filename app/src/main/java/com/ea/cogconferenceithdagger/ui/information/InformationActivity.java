package com.ea.cogconferenceithdagger.ui.information;

import android.os.Bundle;
import android.view.View;

import com.ea.cogconferenceithdagger.R;
import com.ea.cogconferenceithdagger.util.swipefinish.SwipeListener;
import com.ea.cogconferenceithdagger.util.swipefinish.SwipeListenerInterface;

import dagger.android.support.DaggerAppCompatActivity;

public class InformationActivity extends DaggerAppCompatActivity implements SwipeListenerInterface{

    SwipeListener swipeListener ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_information);
        swipeListener = new SwipeListener(this);

        findViewById(R.id.info_swipe).setOnTouchListener(swipeListener);

    }

    @Override
    public void onRightToLeftSwipe(View v) {
    }

    @Override
    public void onLeftToRightSwipe(View v) {
        finish();
    }

    @Override
    public void onTopToBottomSwipe(View v) {

    }

    @Override
    public void onBottomToTopSwipe(View v) {

    }
}
