package com.ea.cogconferenceithdagger.ui.main;

import androidx.annotation.NonNull;
import androidx.core.view.GravityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.navigation.NavController;
import androidx.navigation.NavOptions;
import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;

import android.os.Bundle;
import android.view.MenuItem;

import com.ea.cogconferenceithdagger.BaseActivity;
import com.ea.cogconferenceithdagger.R;
import com.ea.cogconferenceithdagger.databinding.ActivityMainBinding;
import com.google.android.material.navigation.NavigationView;


public class MainActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener {

    private ActivityMainBinding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        setSupportActionBar(binding.toolbar);
        NavController navController = Navigation.findNavController(this,R.id.nav_main_fragment);
        NavigationUI.setupActionBarWithNavController(this,navController,binding.drawerLayout);
        NavigationUI.setupWithNavController(binding.navView,navController);
        binding.navView.setNavigationItemSelectedListener(this);

        showEvents();
    }

    void showEvents(){
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:{
                if(binding.drawerLayout.isDrawerOpen(GravityCompat.START)){
                    binding.drawerLayout.closeDrawer(GravityCompat.START);
                    return true;
                }else {
                    return false;
                }

            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        switch (menuItem.getItemId()){
            case R.id.menu_archive:{
                NavOptions navOptions =new NavOptions.Builder()
                        .setPopUpTo(R.id.main_navigation,true)
                        .build();
                binding.drawerLayout.closeDrawer(GravityCompat.START);
                if(isValidDestination(R.id.archiveFragment))
                Navigation.findNavController(this,R.id.nav_main_fragment).navigate(R.id.archiveFragment);
                break;
            }
            case R.id.menu_settings:{
                binding.drawerLayout.closeDrawer(GravityCompat.START);
                if(isValidDestination(R.id.settingFragment))
                    Navigation.findNavController(this,R.id.nav_main_fragment).navigate(R.id.settingFragment);
                break;
            }
        }
        return true;
    }

    private boolean isValidDestination(int destination){
        return destination != Navigation.findNavController(this,R.id.nav_main_fragment).getCurrentDestination().getId();
    }

    @Override
    public boolean onSupportNavigateUp() {
        return NavigationUI.navigateUp(Navigation.findNavController(this,R.id.nav_main_fragment),binding.drawerLayout);
    }
}
