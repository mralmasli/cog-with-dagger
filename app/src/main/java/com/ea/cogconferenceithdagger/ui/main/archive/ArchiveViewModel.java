package com.ea.cogconferenceithdagger.ui.main.archive;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.LiveDataReactiveStreams;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.ViewModel;

import com.ea.cogconferenceithdagger.model.archive.ArchiveModel;
import com.ea.cogconferenceithdagger.model.events.UserEventsResult;
import com.ea.cogconferenceithdagger.network.archive.ArchiveApi;
import com.ea.cogconferenceithdagger.ui.main.Resource;

import java.util.ArrayList;

import javax.inject.Inject;

import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

public class ArchiveViewModel extends ViewModel {
    private static final String TAG = "ArchiveViewModel";

    private ArchiveApi archiveApi;
    private MediatorLiveData<Resource<ArchiveModel>> userArchive;

    @Inject
    public ArchiveViewModel(ArchiveApi archiveApi) {
        this.archiveApi = archiveApi;
        Log.i(TAG, "ArchiveViewModel: is working");
    }

    public LiveData<Resource<ArchiveModel>> subscribeArchive(){
        if(userArchive == null){
            userArchive = new MediatorLiveData<>();
            userArchive.setValue(Resource.loading(null));

            final LiveData<Resource<ArchiveModel>> source = LiveDataReactiveStreams.fromPublisher(
                    archiveApi.getArchive()
                        .onErrorReturn(throwable -> {
                            ArchiveModel model = new ArchiveModel();
                            UserEventsResult result = new UserEventsResult();
                            result.setCode("-1");
                            ArrayList<UserEventsResult> arrayList = new ArrayList<>();
                            arrayList.add(result);
                            model.setUserEventsResult(arrayList);
                            return model;
                        }).map((Function<ArchiveModel, Resource<ArchiveModel>>) archiveModel -> {
                            if(archiveModel.getUserEventsResult().size()>0){
                                if(archiveModel.getUserEventsResult().get(0).getCode().equals("-1")){
                                    return Resource.error("Something went wrong",null);
                                }
                            }
                            return Resource.success(archiveModel);
                        })
                        .subscribeOn(Schedulers.io())
            );

            userArchive.addSource(source,archiveModelResource -> {
                userArchive.setValue(archiveModelResource);
                userArchive.removeSource(source);
            });
        }
        return userArchive;
    }
}
