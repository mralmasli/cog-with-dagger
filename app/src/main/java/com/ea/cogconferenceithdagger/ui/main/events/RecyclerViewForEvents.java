package com.ea.cogconferenceithdagger.ui.main.events;

import android.graphics.drawable.GradientDrawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ea.cogconferenceithdagger.R;
import com.ea.cogconferenceithdagger.model.events.RVConferencesModel;
import com.ea.cogconferenceithdagger.model.events.UserEventsResult;
import com.ea.cogconferenceithdagger.util.GradientBackground;
import com.ea.cogconferenceithdagger.util.PicassoHandler;
import com.ea.cogconferenceithdagger.util.TimeStamp;
import com.ea.cogconferenceithdagger.util.interfacefor.RVElementOnClick;

import java.util.ArrayList;
import java.util.List;

public class RecyclerViewForEvents extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = "RecyclerViewForEvents";
    private List<RVConferencesModel> conferencesModels = new ArrayList<>();
    private RVElementOnClick rvElementOnClick;

    private PicassoHandler picassoHandler;

    public RecyclerViewForEvents(PicassoHandler picassoHandler) {
        this.picassoHandler = picassoHandler;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if(viewType == 1){
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_for_event_header,parent,false);
            return new HeadViewHolder(view);
        }
        else if(viewType == 3){
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_for_events,parent,false);
            return new EventsViewHolder(view);
        }
        else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_for_events,parent,false);
            return new ContentViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if(holder instanceof HeadViewHolder){
            HeadViewHolder headViewHolder = (HeadViewHolder) holder;

            headViewHolder.setData(conferencesModels.get(position).getData().toString());
        }
        else if(holder instanceof ContentViewHolder){

            ContentViewHolder contentViewHolder = (ContentViewHolder) holder;

            UserEventsResult.Conference data = (UserEventsResult.Conference) conferencesModels.get(position).getData();

            contentViewHolder.setData(data);

        }
        else if(holder instanceof EventsViewHolder){
            EventsViewHolder eventsViewHolder = (EventsViewHolder) holder;

            UserEventsResult.Event event = (UserEventsResult.Event) conferencesModels.get(position).getData();

            eventsViewHolder.content.setText(event.getDescription());

            eventsViewHolder.time.setText(TimeStamp.Converter(event.getToDate()));

            picassoHandler.getPicasso()
                    .load(event.getLogoUrl())
                    .into(eventsViewHolder.image);

            //change background
            eventsViewHolder.linearLayout
                    .setBackground(GradientBackground.gradientDrawable(event.getFromColor(),event.getToColor()));

            //go info activity
            eventsViewHolder.linearLayout.setOnClickListener(v -> {
                if(rvElementOnClick != null)
                    rvElementOnClick.RVOnItemClickListener(v,eventsViewHolder.linearLayout,event);
            });
        }
    }

    @Override
    public int getItemCount() {
        return conferencesModels.size();
    }

    @Override
    public int getItemViewType(int position) {
        if(conferencesModels.get(position).getViewType() == 1){
            return 1;
        }else if(conferencesModels.get(position).getViewType() == 3){
            return 3;
        }
        else {
            return 2;
        }
    }

    public void setConferencesModels(List<RVConferencesModel> models){
        this.conferencesModels = models;
        notifyDataSetChanged();
    }

    public void setOnItemClickListener(RVElementOnClick rvElementOnClick){
        this.rvElementOnClick = rvElementOnClick;
    }

    private class HeadViewHolder extends RecyclerView.ViewHolder{

        private TextView header;
        private HeadViewHolder(@NonNull View itemView) {
            super(itemView);
            header = itemView.findViewById(R.id.event_header);
        }

        public void setData(String data){
            header.setText(data);
        }
    }

    private class ContentViewHolder extends RecyclerView.ViewHolder{
        private ImageView image;
        private TextView content;
        private TextView time;
        private LinearLayout linearLayout;

        private ContentViewHolder(@NonNull View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.main_image);
            content = itemView.findViewById(R.id.main_text_content);
            time = itemView.findViewById(R.id.main_time);
            linearLayout = itemView.findViewById(R.id.main_linear);
        }

        public void setData(UserEventsResult.Conference data){

            content.setText(data.getDescription());

            time.setText(TimeStamp.Converter(data.getToDate()));

            picassoHandler.getPicasso()
                    .load(data.getLogoUrl())
                    .into(image);

            //change background
            linearLayout
                    .setBackground(GradientBackground.gradientDrawable(data.getFromColor(),data.getToColor()));

            //go other activity
            linearLayout.setOnClickListener(v -> {
                if(rvElementOnClick != null)
                    rvElementOnClick.RVOnItemClickListener(v,linearLayout,data);
            });
        }
    }

    private class EventsViewHolder extends RecyclerView.ViewHolder{
        private ImageView image;
        private TextView content;
        private TextView time;
        private LinearLayout linearLayout;
        private EventsViewHolder(@NonNull View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.main_image);
            content = itemView.findViewById(R.id.main_text_content);
            time = itemView.findViewById(R.id.main_time);
            linearLayout = itemView.findViewById(R.id.main_linear);
        }
    }
}
