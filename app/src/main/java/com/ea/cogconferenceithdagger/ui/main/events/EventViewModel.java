package com.ea.cogconferenceithdagger.ui.main.events;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.LiveDataReactiveStreams;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.ViewModel;

import com.ea.cogconferenceithdagger.model.events.ConferencesAndEvents;
import com.ea.cogconferenceithdagger.model.events.UserEventsResult;
import com.ea.cogconferenceithdagger.network.events.EventApi;
import com.ea.cogconferenceithdagger.ui.main.Resource;

import java.util.ArrayList;

import javax.inject.Inject;

import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;


public class EventViewModel extends ViewModel {
    private static final String TAG = "EventViewModel";

    private EventApi  eventApi;
    private MediatorLiveData<Resource<ConferencesAndEvents>> userEvents;


    @Inject
    public EventViewModel(EventApi eventApi) {
        this.eventApi = eventApi;
        Log.d(TAG, "EventViewModel: is working");
    }

    public LiveData<Resource<ConferencesAndEvents>> observerEvents(String lang){
        if(userEvents == null){
                userEvents = new MediatorLiveData<>();
                userEvents.setValue(Resource.loading(null));

                final LiveData<Resource<ConferencesAndEvents>> source = LiveDataReactiveStreams.fromPublisher(
                        eventApi.getEvents(lang)
                                .onErrorReturn(throwable -> {
                                    Log.i(TAG, "observerEvents: error " + throwable.toString());
                                    Log.e(TAG, "observerEvents: ",throwable );
                                    ConferencesAndEvents events = new ConferencesAndEvents();
                                    UserEventsResult result = new UserEventsResult();
                                    result.setCode("-1");
                                    ArrayList<UserEventsResult> arrayList = new ArrayList<>();
                                    arrayList.add(result);
                                    events.setUserEventsResult(arrayList);
                                    return events;
                                })
                                .map((Function<ConferencesAndEvents, Resource<ConferencesAndEvents>>) conferencesAndEvents ->
                                {
                                    Log.i(TAG, "observerEvents: " + conferencesAndEvents.getUserEventsResult().get(0).getCode());
                                    if(conferencesAndEvents.getUserEventsResult().size() > 0) {
                                        if (conferencesAndEvents.getUserEventsResult().get(0).getCode().equals("-1")) {
                                            return Resource.error("Something went wrong", null);
                                        }
                                    }
                                        return Resource.success(conferencesAndEvents);
                                })
                                .subscribeOn(Schedulers.io())
                );

                userEvents.addSource(source, userEventsResultResource -> {
                    userEvents.setValue(userEventsResultResource);
                    userEvents.removeSource(source);
                });
        }

        return userEvents;
    }

    public void refreshData(String lang){
        userEvents = null;
        observerEvents(lang);
    }

    public LiveData<Resource<ConferencesAndEvents>> observerEvents2(String lang){
        return LiveDataReactiveStreams.fromPublisher(
                eventApi.getEvents(lang)
                    .onErrorReturn(throwable -> {
                        Log.i(TAG, "observerEvents: error " + throwable.toString());
                        Log.e(TAG, "observerEvents: ",throwable );
                        ConferencesAndEvents events = new ConferencesAndEvents();
                        UserEventsResult result = new UserEventsResult();
                        result.setCode("-1");
                        ArrayList<UserEventsResult> arrayList = new ArrayList<>();
                        arrayList.add(result);
                        events.setUserEventsResult(arrayList);
                        return events;
                    })
                    .map((Function<ConferencesAndEvents, Resource<ConferencesAndEvents>>) conferencesAndEvents -> {
                        if(conferencesAndEvents.getUserEventsResult().size() > 0) {
                            if (conferencesAndEvents.getUserEventsResult().get(0).getCode().equals("-1")) {
                                return Resource.error("Something went wrong", null);
                            }
                        }
                        return Resource.success(conferencesAndEvents);
                    })
                    .subscribeOn(Schedulers.io())
        );
    }

}
