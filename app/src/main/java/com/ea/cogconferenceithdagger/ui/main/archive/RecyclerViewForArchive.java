package com.ea.cogconferenceithdagger.ui.main.archive;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ea.cogconferenceithdagger.R;
import com.ea.cogconferenceithdagger.model.events.RVConferencesModel;
import com.ea.cogconferenceithdagger.model.events.UserEventsResult;
import com.ea.cogconferenceithdagger.ui.main.events.RecyclerViewForEvents;
import com.ea.cogconferenceithdagger.util.GradientBackground;
import com.ea.cogconferenceithdagger.util.PicassoHandler;

import java.util.ArrayList;
import java.util.List;

public class RecyclerViewForArchive extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = "RecyclerViewForEvents";
    private List<RVConferencesModel> conferencesModels = new ArrayList<>();
    private PicassoHandler picassoHandler;

    public RecyclerViewForArchive(PicassoHandler picassoHandler) {
        this.picassoHandler = picassoHandler;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if(viewType == 1){
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_for_event_header,parent,false);
            return new HeadViewHolder(view);
        }else if(viewType == 3){
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_for_events,parent,false);
            return new EventsViewHolder(view);
        }
        else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_for_events,parent,false);
            return new ContentViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if(holder instanceof HeadViewHolder){
            HeadViewHolder headViewHolder = (HeadViewHolder) holder;
            headViewHolder.header.setText(conferencesModels.get(position).getData().toString());
        }else if(holder instanceof ContentViewHolder){
            ContentViewHolder contentViewHolder = (ContentViewHolder) holder;

            UserEventsResult.Conference  data = (UserEventsResult.Conference) conferencesModels.get(position).getData();

            contentViewHolder.content.setText(data.getDescription());

            contentViewHolder.time.setText(data.getToDate());

            picassoHandler.getPicasso()
                    .load(data.getLogoUrl())
                    .into(contentViewHolder.image);

            contentViewHolder.linearLayout
                    .setBackground(
                            GradientBackground
                                    .gradientDrawable(data.getFromColor(),data.getToColor()));
        }else if(holder instanceof EventsViewHolder){
            EventsViewHolder eventsViewHolder = (EventsViewHolder) holder;

            UserEventsResult.Event event = (UserEventsResult.Event) conferencesModels.get(position).getData();

            eventsViewHolder.content.setText(event.getDescription());

            picassoHandler.getPicasso()
                    .load(event.getLogoUrl())
                    .into(eventsViewHolder.image);

            eventsViewHolder.linearLayout
                    .setBackground(
                            GradientBackground
                                    .gradientDrawable(event.getFromColor(),event.getToColor()));
        }
    }

    @Override
    public int getItemCount() {
        return conferencesModels.size();
    }

    @Override
    public int getItemViewType(int position) {
        if(conferencesModels.get(position).getViewType() == 1){
            return 1;
        }else if(conferencesModels.get(position).getViewType() == 3){
            return 3;
        }
        else {
            return 2;
        }
    }

    public void setConferencesModels(List<RVConferencesModel> models){
        this.conferencesModels = models;
        notifyDataSetChanged();
    }

    private class HeadViewHolder extends RecyclerView.ViewHolder{

        TextView header;
        private HeadViewHolder(@NonNull View itemView) {
            super(itemView);
            header = itemView.findViewById(R.id.event_header);
        }
    }

    private class ContentViewHolder extends RecyclerView.ViewHolder{
        ImageView image;
        TextView content;
        TextView time;
        LinearLayout linearLayout;
        private ContentViewHolder(@NonNull View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.main_image);
            content = itemView.findViewById(R.id.main_text_content);
            time = itemView.findViewById(R.id.main_time);
            linearLayout = itemView.findViewById(R.id.main_linear);
        }
    }

    private class EventsViewHolder extends RecyclerView.ViewHolder{
        private ImageView image;
        private TextView content;
        private TextView time;
        private LinearLayout linearLayout;
        private EventsViewHolder(@NonNull View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.main_image);
            content = itemView.findViewById(R.id.main_text_content);
            time = itemView.findViewById(R.id.main_time);
            linearLayout = itemView.findViewById(R.id.main_linear);
        }
    }
}
