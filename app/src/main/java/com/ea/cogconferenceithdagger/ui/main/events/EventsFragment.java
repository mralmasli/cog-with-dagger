package com.ea.cogconferenceithdagger.ui.main.events;


import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.ea.cogconferenceithdagger.R;
import com.ea.cogconferenceithdagger.databinding.FragmentEventsBinding;
import com.ea.cogconferenceithdagger.model.events.RVConferencesModel;
import com.ea.cogconferenceithdagger.ui.information.InformationActivity;
import com.ea.cogconferenceithdagger.util.PicassoHandler;
import com.ea.cogconferenceithdagger.util.intent.ChangeActivity;
import com.ea.cogconferenceithdagger.util.interfacefor.RVElementOnClick;
import com.ea.cogconferenceithdagger.viewmodel.ViewModelProviderFactory;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import dagger.android.support.DaggerFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class EventsFragment extends DaggerFragment {

    private static final String TAG = "EventsFragment";

    public EventsFragment() {
        // Required empty public constructor
    }

    private FragmentEventsBinding binding;

    private EventViewModel eventViewModel;

    private ArrayList<RVConferencesModel> model;

    @Inject
    RecyclerViewForEvents adapterForEvents;

    @Inject
    ViewModelProviderFactory providerFactory;

    @Inject
    PicassoHandler picassoHandler;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater,R.layout.fragment_events, container, false);
        View view = binding.getRoot();

        eventViewModel = ViewModelProviders.of(getActivity(),providerFactory).get(EventViewModel.class);

        initRV();

        subscribeObserver();

        swipeRefresh();

        return view;
    }

    private void subscribeObserver(){
        eventViewModel.observerEvents("az").removeObservers(getViewLifecycleOwner());
        eventViewModel.observerEvents("az").
                observe(getViewLifecycleOwner(),conferencesAndEventsResource -> {
                    if(conferencesAndEventsResource != null){
                        switch (conferencesAndEventsResource.status){
                            case LOADING:{
                                Log.i(TAG, "subscribeObserver: isLoading");
                                binding.swipeEvents.setRefreshing(true);
                                break;
                            }
                            case ERROR:{
                                Log.i(TAG, "subscribeObserver: haveError " + conferencesAndEventsResource.message);
                                binding.swipeEvents.setRefreshing(false);
                                Snackbar.make(getActivity().findViewById(android.R.id.content),getString(R.string.check_internet),Snackbar.LENGTH_SHORT).show();
                                break;
                            }
                            case SUCCESS:{
                                Log.i(TAG, "subscribeObserver: isLoaded");
                                model = new ArrayList<>();
                                model.add(new RVConferencesModel(1,"Conferences",1));
                                for(int i = 0; i < conferencesAndEventsResource.data.getUserEventsResult().get(0).getConferences().size();i++){
                                    model.add(new RVConferencesModel(2,
                                            conferencesAndEventsResource.data.getUserEventsResult().get(0).getConferences().get(i),
                                            1));
                                }
                                model.add(new RVConferencesModel(1,"Events",1));
                                for(int i = 0; i < conferencesAndEventsResource.data.getUserEventsResult().get(0).getEvents().size();i++){
                                    model.add(new RVConferencesModel(3,
                                            conferencesAndEventsResource.data.getUserEventsResult().get(0).getEvents().get(i),
                                            1));
                                }

                                adapterForEvents.setConferencesModels(model);
                                binding.swipeEvents.setRefreshing(false);
                                break;
                            }
                        }
                }
        });
    }


    private void initRV(){
        adapterForEvents = new RecyclerViewForEvents(picassoHandler);
        adapterForEvents.setOnItemClickListener((v, layout, data) -> {
            ChangeActivity.passIntent(getContext(), InformationActivity.class);
        });
        binding.rvEvent.setLayoutManager(new LinearLayoutManager(getContext()));
        binding.rvEvent.setAdapter(adapterForEvents);
    }

    private void swipeRefresh(){
        binding.swipeEvents.setOnRefreshListener(() -> {
            eventViewModel.refreshData("az");
            subscribeObserver();
        });
    }
}
