package com.ea.cogconferenceithdagger.di;

import androidx.lifecycle.ViewModelProvider;

import com.ea.cogconferenceithdagger.viewmodel.ViewModelProviderFactory;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class ViewModelFactoryModule {
    @Binds
    public abstract ViewModelProvider.Factory bindViewModelFactory(ViewModelProviderFactory providerFactory);
}
