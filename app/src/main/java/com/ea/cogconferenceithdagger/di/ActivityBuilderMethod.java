package com.ea.cogconferenceithdagger.di;

import com.ea.cogconferenceithdagger.di.main.ArchiveApiModule;
import com.ea.cogconferenceithdagger.di.main.EventApiModule;
import com.ea.cogconferenceithdagger.di.main.MainFragmentBuilderModule;
import com.ea.cogconferenceithdagger.di.main.MainViewModelModule;
import com.ea.cogconferenceithdagger.ui.information.InformationActivity;
import com.ea.cogconferenceithdagger.ui.main.MainActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityBuilderMethod {

    @ContributesAndroidInjector(
            modules = {
                    MainFragmentBuilderModule.class,
                    MainViewModelModule.class,
                    EventApiModule.class,
                    ArchiveApiModule.class,
            }
    )
    abstract MainActivity contributeMainActivity();

    @ContributesAndroidInjector
    abstract InformationActivity contributeInformationActivity();
}
