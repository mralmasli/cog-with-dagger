package com.ea.cogconferenceithdagger.di.main;

import com.ea.cogconferenceithdagger.network.events.EventApi;
import com.ea.cogconferenceithdagger.ui.main.events.RecyclerViewForEvents;
import com.ea.cogconferenceithdagger.util.PicassoHandler;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

@Module
public class EventApiModule {

    @Provides
    RecyclerViewForEvents provideRVEvents(PicassoHandler picassoHandler){
        return new RecyclerViewForEvents(picassoHandler);
    }

    @Provides
    static EventApi provideEventApi(Retrofit retrofit){
        return retrofit.create(EventApi.class);
    }
}
