package com.ea.cogconferenceithdagger.di.main;

import androidx.lifecycle.ViewModel;

import com.ea.cogconferenceithdagger.di.ViewModelKey;
import com.ea.cogconferenceithdagger.ui.main.archive.ArchiveViewModel;
import com.ea.cogconferenceithdagger.ui.main.events.EventViewModel;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

@Module
public abstract class MainViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(EventViewModel.class)
    public abstract ViewModel bindEventViewModel(EventViewModel eventViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(ArchiveViewModel.class)
    public abstract ViewModel bindArchiveViewModel(ArchiveViewModel archiveViewModel);
}
