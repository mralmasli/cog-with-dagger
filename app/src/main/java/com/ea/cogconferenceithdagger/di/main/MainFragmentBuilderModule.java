package com.ea.cogconferenceithdagger.di.main;

import com.ea.cogconferenceithdagger.ui.main.archive.ArchiveFragment;
import com.ea.cogconferenceithdagger.ui.main.events.EventsFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class MainFragmentBuilderModule {
    @ContributesAndroidInjector
    abstract EventsFragment contributeEventsFragment();

    @ContributesAndroidInjector
    abstract ArchiveFragment contributeArchiveFragment();
}
