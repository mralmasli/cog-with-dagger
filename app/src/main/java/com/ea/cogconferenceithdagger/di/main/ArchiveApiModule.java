package com.ea.cogconferenceithdagger.di.main;

import com.ea.cogconferenceithdagger.network.archive.ArchiveApi;
import com.ea.cogconferenceithdagger.ui.main.archive.RecyclerViewForArchive;
import com.ea.cogconferenceithdagger.util.PicassoHandler;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

@Module
public class ArchiveApiModule {

    @Provides
    RecyclerViewForArchive provideRVArchive(PicassoHandler picassoHandler){
        return new RecyclerViewForArchive(picassoHandler);
    }

    @Provides
    static ArchiveApi provideArchiveApi(Retrofit retrofit){
        return retrofit.create(ArchiveApi.class);
    }
}
