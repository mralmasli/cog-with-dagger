package com.ea.cogconferenceithdagger.network.events;

import com.ea.cogconferenceithdagger.model.events.ConferencesAndEvents;

import io.reactivex.Flowable;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface EventApi {
    @GET("user_events/{lang}")
    Flowable<ConferencesAndEvents> getEvents(
            @Path("lang") String lang
    );

}
