package com.ea.cogconferenceithdagger.network.archive;

import com.ea.cogconferenceithdagger.model.archive.ArchiveModel;

import io.reactivex.Flowable;
import retrofit2.http.GET;

public interface ArchiveApi {
    //{data} when you add element
    @GET("user_archive/token/az")
    Flowable<ArchiveModel> getArchive();
}
