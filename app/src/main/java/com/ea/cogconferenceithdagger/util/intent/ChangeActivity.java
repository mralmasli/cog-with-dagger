package com.ea.cogconferenceithdagger.util.intent;

import android.content.Context;
import android.content.Intent;

import com.ea.cogconferenceithdagger.util.Constants;

public class ChangeActivity {

    public static void passIntent(Context context, Class c){
        Intent go = new Intent(context,c);
        context.startActivity(go);
    }

    public static void passIntent(Context context, Class c,String data){
        Intent go = new Intent(context,c);
        go.putExtra(Constants.EXTRA_DATA, data);
        context.startActivity(go);
    }
}
