package com.ea.cogconferenceithdagger.util.interfacefor;

import android.view.View;
import android.widget.LinearLayout;

public interface RVElementOnClick {
    void RVOnItemClickListener(View v, LinearLayout layout,Object data);
}
