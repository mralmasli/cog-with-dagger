package com.ea.cogconferenceithdagger.util;

import android.annotation.SuppressLint;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TimeStamp {

    @SuppressLint("SimpleDateFormat")
    public static String Converter(String data){
        try {
            SimpleDateFormat spf = new SimpleDateFormat("yyyy-MM-dd"); //date format in which your current string is
            Date newDate = null;
            newDate = spf.parse(data);
            spf = new SimpleDateFormat("dd MMM yyyy"); //date format in which you want to convert
            data = spf.format(newDate);
            //System.out.println(dt);

            //Log.e("FRM_DT", dt);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return data;
    }

}
