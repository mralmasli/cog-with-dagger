package com.ea.cogconferenceithdagger.util;

import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;

public class GradientBackground {

    public static GradientDrawable gradientDrawable (String begin,String end){
        String[] part = begin.split("~");

        String[] endP = end.split("~");

        GradientDrawable gradientDrawable = new GradientDrawable(
                GradientDrawable.Orientation.TOP_BOTTOM,
                new int[]{
                        Color.rgb(Integer.parseInt(part[0].trim()),Integer.parseInt(part[1].trim()),Integer.parseInt(part[2].trim())),
                        Color.rgb(Integer.parseInt(endP[0].trim()),Integer.parseInt(endP[1].trim()),Integer.parseInt(endP[2].trim()))
                }
        );
        gradientDrawable.setCornerRadius(15);

        return gradientDrawable;
    }
}
