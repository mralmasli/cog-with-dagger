package com.ea.cogconferenceithdagger.model.archive;

import com.ea.cogconferenceithdagger.model.events.UserEventsResult;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ArchiveModel {
    @SerializedName("user_archiveResult")
    @Expose
    private List<UserEventsResult> userEventsResult;

    public List<UserEventsResult> getUserEventsResult() {
        return userEventsResult;
    }

    public void setUserEventsResult(List<UserEventsResult> userEventsResult) {
        this.userEventsResult = userEventsResult;
    }
}
