package com.ea.cogconferenceithdagger.model.events;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class UserEventsResult {

    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("conferences")
    @Expose
    private List<Conference> conferences ;
    @SerializedName("events")
    @Expose
    private List<Event> events ;
    @SerializedName("message")
    @Expose
    private String message;

    public UserEventsResult() {
    }

    public UserEventsResult(String code, List<Conference> conferences, List<Event> events, String message) {
        this.code = code;
        this.conferences = conferences;
        this.events = events;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public List<Conference> getConferences() {
        return conferences;
    }

    public void setConferences(List<Conference> conferences) {
        this.conferences = conferences;
    }

    public List<Event> getEvents() {
        return events;
    }

    public void setEvents(List<Event> events) {
        this.events = events;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


    public class Event {

        @SerializedName("country")
        @Expose
        private String country;
        @SerializedName("date")
        @Expose
        private String date;
        @SerializedName("description")
        @Expose
        private String description;
        @SerializedName("from_color")
        @Expose
        private String fromColor;
        @SerializedName("from_date")
        @Expose
        private String fromDate;
        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("logo_url")
        @Expose
        private String logoUrl;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("to_color")
        @Expose
        private String toColor;
        @SerializedName("to_date")
        @Expose
        private String toDate;

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getFromColor() {
            return fromColor;
        }

        public void setFromColor(String fromColor) {
            this.fromColor = fromColor;
        }

        public String getFromDate() {
            return fromDate;
        }

        public void setFromDate(String fromDate) {
            this.fromDate = fromDate;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getLogoUrl() {
            return logoUrl;
        }

        public void setLogoUrl(String logoUrl) {
            this.logoUrl = logoUrl;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getToColor() {
            return toColor;
        }

        public void setToColor(String toColor) {
            this.toColor = toColor;
        }

        public String getToDate() {
            return toDate;
        }

        public void setToDate(String toDate) {
            this.toDate = toDate;
        }

    }

    public class Conference {

        @SerializedName("country")
        @Expose
        private String country;
        @SerializedName("date")
        @Expose
        private String date;
        @SerializedName("description")
        @Expose
        private String description;
        @SerializedName("from_color")
        @Expose
        private String fromColor;
        @SerializedName("from_date")
        @Expose
        private String fromDate;
        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("logo_url")
        @Expose
        private String logoUrl;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("to_color")
        @Expose
        private String toColor;
        @SerializedName("to_date")
        @Expose
        private String toDate;

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getFromColor() {
            return fromColor;
        }

        public void setFromColor(String fromColor) {
            this.fromColor = fromColor;
        }

        public String getFromDate() {
            return fromDate;
        }

        public void setFromDate(String fromDate) {
            this.fromDate = fromDate;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getLogoUrl() {
            return logoUrl;
        }

        public void setLogoUrl(String logoUrl) {
            this.logoUrl = logoUrl;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getToColor() {
            return toColor;
        }

        public void setToColor(String toColor) {
            this.toColor = toColor;
        }

        public String getToDate() {
            return toDate;
        }

        public void setToDate(String toDate) {
            this.toDate = toDate;
        }

    }
}
