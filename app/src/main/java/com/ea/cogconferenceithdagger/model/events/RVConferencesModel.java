package com.ea.cogconferenceithdagger.model.events;

public class RVConferencesModel {
    private int viewType;
    private Object data;
    private int size;

    public RVConferencesModel() {
    }

    public RVConferencesModel(int viewType, Object data,int size) {
        this.viewType = viewType;
        this.data = data;
        this.size = size;
    }

    public int getViewType() {
        return viewType;
    }

    public void setViewType(int viewType) {
        this.viewType = viewType;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }
}
