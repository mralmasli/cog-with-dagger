package com.ea.cogconferenceithdagger.model.events;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ConferencesAndEvents {

    @SerializedName("user_eventsResult")
    @Expose
    private List<UserEventsResult> userEventsResult;

    public List<UserEventsResult> getUserEventsResult() {
        return userEventsResult;
    }

    public void setUserEventsResult(List<UserEventsResult> userEventsResult) {
        this.userEventsResult = userEventsResult;
    }

}